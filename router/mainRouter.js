const express = require('express');
const mainRouter = express.Router();
const {user_game_biodata, user_game_history, user_game} = require('../models')


//LANDING PAGE
mainRouter.get('/', (req, res) => {
    res.render('index.ejs');
})

mainRouter.get('/game', (req, res) => {
    res.sendFile('/home/han/MEGA/src/GIT/BINAR/BINAR_challenge/public/rps/game.html')
});


//LOGIN PAGE
mainRouter.get('/login', (req, res) => {
    res.render('login')
});

mainRouter.post('/api/login', (req,res) => {
    //validation
    const {username, password} = req.body;

})

//===============================================================================
// 
//===============================================================================

//SIGNUP PAGE
mainRouter.get('/register', (req, res) => {
    res.render('register')
});

mainRouter.post('/api/register', (req, res) => {
    const {username, birthdate, address} = req.body

    if (username === undefined || birthdate === undefined || address === undefined) {
        return res.status(400).json("Tolong isi semua field.")
    }
    
    user_game_biodata.create({
        username: username,
        birthdate: birthdate,
        address : address
    }).then(user_game_biodata => {
        res.status(200).render('dashboard')    
        
        
    }).catch(err => {
        console.log(err)
        res.status(500).status(500).json("Internal server error")
    });
});


module.exports = mainRouter;