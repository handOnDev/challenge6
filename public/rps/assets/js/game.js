function pilihancomp () {
    const handcomp = ["rock","scissor","paper"]
    const comp = Math.floor(Math.random() * 3)
    return handcomp [comp]
}

function hasil(comp, player) {
    if (player == comp) return 'Draw'
    if (player == 'rock') return (comp == 'scissor') ? 'Player Win' : 'Computer Win'
    if (player == 'scissor') return (comp == 'paper') ? 'Player Win' : 'Computer Win'
    if (player == 'paper') return (comp == 'rock') ? 'Player Win' : 'Computer Win'
}

const player_pick = document.querySelectorAll('li img.player')
player_pick.forEach(function(pilihan){
    pilihan.addEventListener('click', function(){
        pilihan.style.backgroundColor = '#C4C4C4'
        pilihan.style.borderRadius = '20%'

        const pilcomp = pilihancomp()
        const pilPlayer = pilihan.classList.item(2)
        const hasilAkhir = hasil(pilcomp, pilPlayer)

        const showcomp = document.querySelector('img.comp.' + pilcomp)
        showcomp.style.backgroundColor = '#C4C4C4'
        showcomp.style.borderRadius = '20%'

        const status  = document.querySelector('.check')
        const vs = status.querySelector('.vs')
        status.removeChild(vs)

        const tampilkanHasil = document.querySelector('.hasil')
        tampilkanHasil.classList.add('akhir')
        tampilkanHasil.innerHTML = hasilAkhir 

        for (let  i=0; i < player_pick.length; i++){
            player_pick[i].classList.add('noclick')
        }


    })
})

const refresh = document.querySelector('.refresh')
refresh.addEventListener('click', function(){
    location.reload()
})

