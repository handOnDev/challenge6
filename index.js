const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const mainRouter = require('./router/mainRouter');

app.use(express.json());
app.use(express.urlencoded({extended: false}));

// view engine
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'))
app.use(mainRouter);

app.listen(PORT, () => {
    console.log('Now listening on port ' + PORT)
});